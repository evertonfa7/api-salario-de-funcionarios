# API Salário de Funcinários
## API que mostra qual funcionário tem o maior salário

---

- Para executar basta rodar: <br>
`npm install` (para instalar as depêndencias)

- E depois para subir o servidor: <br>
`npm start`

---
## Exemplo de requisição
<img src="docs/print-requisição.png" width="900px">


## Para acessar a documentação:
`http://localhost:3333/doc`

const express = require("express");
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../docs/swagger.json');
const app = express();
const PORT = 3333;

app.use(express.json());
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get("/", (req, res) => {
  return res.send("Bem-vindo a API");
});

app.post("/funcionario/maiorsalario", (req, res) => {
  const arrFuncionarios = req.body;

  let maiorSalario = 0;
  let funcionarioComMaiorSalario;

  arrFuncionarios.forEach((funcionario) => {
    if (funcionario.salario > maiorSalario) {
      maiorSalario = funcionario.salario;
      funcionarioComMaiorSalario = funcionario;
    }
  });

  return res.json(funcionarioComMaiorSalario);
});

app.listen(PORT, () => {
  console.log(`Server is running on ${PORT}`);
});
